package ru.basa62.stepikJava.week3.lesson5.W3L5S09;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W3L5S09Test {
    private final String[] tests = {
            "This comment is so good.",                            // OK
            "This comment is so Loooooooooooooooooooooooooooong.", // TOO_LONG
            "Very negative comment !!!!=(!!!!,",                   // NEGATIVE_TEXT
            "Very BAAAAAAAAAAAAAAAAAAAAAAAAD comment with :|,",    // NEGATIVE_TEXT or TOO_LONG
            "This comment is so bad....",                          // SPAM
            "The comment is a spam, maybeeeeeeeeeeeeeeeeeeeeee!",  // SPAM or TOO_LONG
            "Negative bad :( spam.",                               // SPAM or NEGATIVE_TEXT
            "Very bad, very neg =(, very .................."       // SPAM or NEGATIVE_TEXT or TOO_LONG
    };

    private final SpamAnalyzer spamAnalyzer = new SpamAnalyzer(new String[]{"spam", "bad"});
    private final TooLongTextAnalyzer tooLongTextAnalyzer = new TooLongTextAnalyzer(40);
    private final NegativeTextAnalyzer negativeTextAnalyzer = new NegativeTextAnalyzer();

    @Test
    void spamAnalyzerTest() {
        assertEquals(Label.OK, spamAnalyzer.processText(tests[0]));
        assertEquals(Label.OK, spamAnalyzer.processText(tests[1]));
        assertEquals(Label.OK, spamAnalyzer.processText(tests[2]));
        assertEquals(Label.OK, spamAnalyzer.processText(tests[3]));
        assertEquals(Label.SPAM, spamAnalyzer.processText(tests[4]));
        assertEquals(Label.SPAM, spamAnalyzer.processText(tests[5]));
        assertEquals(Label.SPAM, spamAnalyzer.processText(tests[6]));
        assertEquals(Label.SPAM, spamAnalyzer.processText(tests[7]));
    }

    @Test
    void tooLongTextAnalyzerTest() {
        assertEquals(Label.OK, tooLongTextAnalyzer.processText(tests[0]));
        assertEquals(Label.TOO_LONG, tooLongTextAnalyzer.processText(tests[1]));
        assertEquals(Label.OK, tooLongTextAnalyzer.processText(tests[2]));
        assertEquals(Label.TOO_LONG, tooLongTextAnalyzer.processText(tests[3]));
        assertEquals(Label.OK, tooLongTextAnalyzer.processText(tests[4]));
        assertEquals(Label.TOO_LONG, tooLongTextAnalyzer.processText(tests[5]));
        assertEquals(Label.OK, tooLongTextAnalyzer.processText(tests[6]));
        assertEquals(Label.TOO_LONG, tooLongTextAnalyzer.processText(tests[7]));
    }

    @Test
    void negativeTextAnalyzerTest() {
        assertEquals(Label.OK, negativeTextAnalyzer.processText(tests[0]));
        assertEquals(Label.OK, negativeTextAnalyzer.processText(tests[1]));
        assertEquals(Label.NEGATIVE_TEXT, negativeTextAnalyzer.processText(tests[2]));
        assertEquals(Label.NEGATIVE_TEXT, negativeTextAnalyzer.processText(tests[3]));
        assertEquals(Label.OK, negativeTextAnalyzer.processText(tests[4]));
        assertEquals(Label.OK, negativeTextAnalyzer.processText(tests[5]));
        assertEquals(Label.NEGATIVE_TEXT, negativeTextAnalyzer.processText(tests[6]));
        assertEquals(Label.NEGATIVE_TEXT, negativeTextAnalyzer.processText(tests[7]));
    }

    @Test
    void checkLabels() {
        W3L5S09 s = new W3L5S09();
        assertEquals(Label.OK, s.checkLabels(new TextAnalyzer[]{spamAnalyzer, tooLongTextAnalyzer, negativeTextAnalyzer}, tests[0]));

        assertEquals(Label.SPAM, s.checkLabels(new TextAnalyzer[]{spamAnalyzer, tooLongTextAnalyzer, negativeTextAnalyzer}, tests[7]));
        assertEquals(Label.TOO_LONG, s.checkLabels(new TextAnalyzer[]{tooLongTextAnalyzer, spamAnalyzer, negativeTextAnalyzer}, tests[7]));
        assertEquals(Label.NEGATIVE_TEXT, s.checkLabels(new TextAnalyzer[]{negativeTextAnalyzer, spamAnalyzer, tooLongTextAnalyzer}, tests[7]));
    }
}