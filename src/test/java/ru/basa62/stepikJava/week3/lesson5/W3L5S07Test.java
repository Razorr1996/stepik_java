package ru.basa62.stepikJava.week3.lesson5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static ru.basa62.stepikJava.week3.lesson5.W3L5S07.integrate;

class W3L5S07Test {

    @Test
    void integrateTest() {
        assertEquals(10.0, integrate(x -> 1, 0, 10), 1e-6);
        assertEquals(70.0, integrate(x -> x + 2, 0, 10), 1e-6);
        assertEquals(0.603848, integrate(x -> Math.sin(x) / x, 1, 5), 1e-6);
    }
}