package ru.basa62.stepikJava.week3.lesson5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static ru.basa62.stepikJava.week3.lesson5.W3L5S08.AsciiCharSequence;

class W3L5S08Test {

    @Test
    void asciiCharSequence() {
        byte[] example = {72, 101, 108, 108, 111, 33};
        AsciiCharSequence answer = new AsciiCharSequence(example);
        assertEquals("Hello!", answer.toString());
        assertEquals(6, answer.length());
        assertEquals('e', answer.charAt(1));
        assertEquals("ello", answer.subSequence(1, 5).toString());

        //проверка на нарушение инкапсуляции private поля
        example[0] = 74;
        assertEquals("Hello!", answer.toString());
    }
}
