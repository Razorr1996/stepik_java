package ru.basa62.stepikJava.week3.lesson4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static ru.basa62.stepikJava.week3.lesson4.W3L4S09.ComplexNumber;

class W3L4S09Test {

    private final ComplexNumber cn1 = new ComplexNumber(2.3412, 7.2434);
    private final ComplexNumber cn2 = new ComplexNumber(2.3412, 7.2434);
    private final ComplexNumber cn3 = new ComplexNumber(3.23124, 1435.23);

    @Test
    void testEquals() {
        assertEquals(cn1, cn2);
        assertEquals(cn2, cn1);

        assertNotEquals(cn1, cn3);
        assertNotEquals(cn3, cn1);

        assertNotEquals(cn2, cn3);
        assertNotEquals(cn3, cn2);
    }

    @Test
    void testHashCode() {
        assertEquals(cn1.hashCode(), cn2.hashCode());
        assertEquals(cn2.hashCode(), cn1.hashCode());

        assertNotEquals(cn1.hashCode(), cn3.hashCode());
        assertNotEquals(cn3.hashCode(), cn1.hashCode());

        assertNotEquals(cn2.hashCode(), cn3.hashCode());
        assertNotEquals(cn3.hashCode(), cn2.hashCode());
    }
}