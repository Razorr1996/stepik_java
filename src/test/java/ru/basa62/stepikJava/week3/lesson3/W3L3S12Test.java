package ru.basa62.stepikJava.week3.lesson3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W3L3S12Test {

    @Test
    void moveRobot() {
        for (int i = -5; i <= 5; i++) {
            for (int j = -5; j <= 5; j++) {
                Robot robot = new Robot();
                W3L3S12.moveRobot(robot, i, j);
                assertEquals(i, robot.getX());
                assertEquals(j, robot.getY());
            }
        }
    }
}