package ru.basa62.stepikJava.week4.lesson2.W4L2S07;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W4L2S07Test {
    static class RobotConnectionTestImpl implements RobotConnection {
        private final boolean moveException;
        private final boolean closeException;

        public RobotConnectionTestImpl() {
            this(false, false);
        }

        public RobotConnectionTestImpl(boolean moveException) {
            this(moveException, false);
        }

        public RobotConnectionTestImpl(boolean moveException, boolean closeException) {
            this.moveException = moveException;
            this.closeException = closeException;
        }

        @Override
        public void moveRobotTo(int x, int y) {
            if (moveException) {
                throw new RobotConnectionException(this.getClass().getName());
            }
        }

        @Override
        public void close() {
            if (closeException) {
                throw new RobotConnectionException(this.getClass().getName());
            }
        }
    }

    static class RobotConnectionManagerTestImp implements RobotConnectionManager {
        private final int failureCount;

        private final boolean moveException;
        private final boolean closeException;

        private int currentFailureCount = 0;

        RobotConnectionManagerTestImp() {
            this(0, false, false);
        }

        RobotConnectionManagerTestImp(int failureCount) {
            this(failureCount, false, false);
        }

        RobotConnectionManagerTestImp(boolean moveException, boolean closeException) {
            this(0, moveException, closeException);
        }

        RobotConnectionManagerTestImp(int failureCount, boolean moveException, boolean closeException) {
            this.failureCount = failureCount;
            this.moveException = moveException;
            this.closeException = closeException;
        }

        @Override
        public RobotConnection getConnection() {
            if (currentFailureCount < failureCount) {
                currentFailureCount += 1;
                throw new RobotConnectionException(this.getClass().getName());
            }
            return new RobotConnectionTestImpl(moveException, closeException);
        }
    }

    @Test
    void moveRobotNormal() {
        RobotConnectionManager manager = new RobotConnectionManagerTestImp();
        assertDoesNotThrow(() -> W4L2S07.moveRobot(manager, 1, 1));
    }

    @Test
    void moveRobot1Retry() {
        RobotConnectionManager manager = new RobotConnectionManagerTestImp(1);
        assertDoesNotThrow(() -> W4L2S07.moveRobot(manager, 1, 1));
    }

    @Test
    void moveRobot2Retry() {
        RobotConnectionManager manager = new RobotConnectionManagerTestImp(2);
        assertDoesNotThrow(() -> W4L2S07.moveRobot(manager, 1, 1));
    }

    @Test
    void moveRobot3Retry() {
        RobotConnectionManager manager = new RobotConnectionManagerTestImp(3);
        assertThrows(RobotConnectionException.class, () -> W4L2S07.moveRobot(manager, 1, 1));
    }

    @Test
    void moveRobot10Retry() {
        RobotConnectionManager manager = new RobotConnectionManagerTestImp(10);
        assertThrows(RobotConnectionException.class, () -> W4L2S07.moveRobot(manager, 1, 1));
    }

    @Test
    void moveRobotMoveException() {
        RobotConnectionManager manager = new RobotConnectionManagerTestImp(true, false);
        assertThrows(RobotConnectionException.class, () -> W4L2S07.moveRobot(manager, 1, 1));
    }

    @Test
    void moveRobotCloseException() {
        RobotConnectionManager manager = new RobotConnectionManagerTestImp(false, true);
        assertDoesNotThrow(() -> W4L2S07.moveRobot(manager, 1, 1));
    }

    @Test
    void moveRobotMoveAndCloseException() {
        RobotConnectionManager manager = new RobotConnectionManagerTestImp(true, true);
        assertThrows(RobotConnectionException.class, () -> W4L2S07.moveRobot(manager, 1, 1));
    }
}
