package ru.basa62.stepikJava.week4.lesson3;

import org.junit.jupiter.api.Test;

import java.util.logging.Logger;

import static ru.basa62.stepikJava.week4.lesson3.W4L3S09.*;
import static org.junit.jupiter.api.Assertions.*;

class W4L3S09Test {
    @Test
    void testSpyNoChanges() {
        Logger logger = Logger.getLogger(this.getClass().getName());
        Spy spy = new Spy(logger);

        Sendable clearMail = new MailMessage("Artem", "Mari", "Mail body");
        Sendable processedMail = spy.processMail(clearMail);
        assertEquals(clearMail, processedMail);
    }

    @Test
    void testThiefLessPrice() {
        Thief thief = new Thief(10);
        MailPackage mailPackage = new MailPackage("Artem", "Mari", new W4L3S09.Package("Box", 5));
        Sendable processedMail = thief.processMail(mailPackage);
        assertEquals(mailPackage, processedMail);
        assertEquals(0, thief.getStolenValue());
    }

    @Test
    void testThiefMorePrice() {
        Thief thief = new Thief(10);
        MailPackage mailPackage = new MailPackage("Artem", "Mari", new W4L3S09.Package("iPhone", 10));
        Sendable processedMail = thief.processMail(mailPackage);
        assertEquals(new MailPackage("Artem", "Mari", new W4L3S09.Package("stones instead of iPhone", 0)), processedMail);
        assertEquals(10, thief.getStolenValue());
    }

    @Test
    void testInspector() {
        Thief thief = new Thief(10);
        MailPackage mailPackage = new MailPackage("Artem", "Mari", new W4L3S09.Package("iPhone", 10));
        Sendable processedMail = thief.processMail(mailPackage);

        Inspector inspector = new Inspector();
        assertDoesNotThrow(() -> inspector.processMail(mailPackage));
        assertThrows(StolenPackageException.class, () -> inspector.processMail(processedMail));
        assertThrows(IllegalPackageException.class, () -> inspector.processMail(new MailPackage("Artem", "Mari", new W4L3S09.Package("banned substance", 1000))));
    }
}