package ru.basa62.stepikJava.week4.lesson1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W4L1S10Test {

    @Test
    void getCallerClassAndMethodName() {
        assertEquals("ru.basa62.stepikJava.week4.lesson1.W4L1S10Test#outerMethod", outerMethod());
    }

    String outerMethod() {
        return innerMethod();
    }

    String innerMethod() {
        return W4L1S10.getCallerClassAndMethodName();
    }
}