package ru.basa62.stepikJava.week4.lesson1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W4L1S08Test {

    @Test
    void main() {
        assertThrows(ClassCastException.class, () -> W4L1S08.main(new String[]{}));
    }
}