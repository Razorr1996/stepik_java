package ru.basa62.stepikJava.week4.lesson1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W4L1S09Test {

    @Test
    void sqrt() {
        assertEquals(0.0, W4L1S09.sqrt(0.0), 1e-6);
        assertEquals(1.0, W4L1S09.sqrt(1.0), 1e-6);
        assertEquals(2.0, W4L1S09.sqrt(4.0), 1e-6);
        assertEquals(10.0, W4L1S09.sqrt(100.0), 1e-6);

        RuntimeException exception = assertThrows(IllegalArgumentException.class, () -> W4L1S09.sqrt(-1.0));
        assertEquals("Expected non-negative number, got -1.0", exception.getMessage());
    }
}