package ru.basa62.stepikJava.week5.lesson4.W5L4S08;

import java.io.*;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class W5L4S08Test {

    private static byte[] serialize(int count, Object[] objects) throws IOException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteStream);
        out.writeInt(count);
        for (Object object : objects) {
            out.writeObject(object);
        }
        out.flush();
        return byteStream.toByteArray();
    }

    @Test
    @Timeout(8)
    public void test1DeserializeAnimalArrayNormalZero() throws Throwable {
        Animal[] testAnimals = new Animal[0];
        byte[] data = serialize(0, testAnimals);
        Animal[] actualAnimals = W5L4S08.deserializeAnimalArray(data);
        assertNotNull(actualAnimals);
        assertArrayEquals(testAnimals, actualAnimals);
    }

    @Test
    @Timeout(8)
    public void test2DeserializeAnimalArrayNormalOne() throws Throwable {
        Animal[] testAnimals = new Animal[]{
                new Animal("Cat")
        };
        byte[] data = serialize(1, testAnimals);
        Animal[] actualAnimals = W5L4S08.deserializeAnimalArray(data);
        assertNotNull(actualAnimals);
        assertArrayEquals(testAnimals, actualAnimals);
    }

    @Test
    @Timeout(8)
    public void test3DeserializeAnimalArrayNormalThee() throws Throwable {
        Animal[] testAnimals = new Animal[]{
                new Animal("Cat"),
                new Animal("Dog"),
                new Animal("Mouse")
        };
        byte[] data = serialize(testAnimals.length, testAnimals);
        Animal[] actualAnimals = W5L4S08.deserializeAnimalArray(data);
        assertNotNull(actualAnimals);
        assertArrayEquals(testAnimals, actualAnimals);
    }

    @Test
    @Timeout(8)
    public void test4DeserializeAnimalArrayNotAnimal() throws Throwable {
        Object[] testAnimals = new Object[]{
                100
        };
        byte[] data = serialize(testAnimals.length, testAnimals);
        assertThrows(IllegalArgumentException.class, () -> {
            Animal[] animals = W5L4S08.deserializeAnimalArray(data);
        });
    }

    @Test
    @Timeout(8)
    public void test5DeserializeAnimalArrayWrongCount() throws Throwable {
        Animal[] testAnimals = new Animal[]{
                new Animal("Cat")
        };
        byte[] data = serialize(2, testAnimals);
        assertThrows(IllegalArgumentException.class, () -> {
            Animal[] animals = W5L4S08.deserializeAnimalArray(data);
        });
    }

    @Test
    @Timeout(8)
    public void test6DeserializeAnimalArrayIncorrectCount() throws Throwable {
        Animal[] testAnimals = new Animal[]{
                new Animal("Cat")
        };
        byte[] data = serialize(-10, testAnimals);
        assertThrows(IllegalArgumentException.class, () -> {
            Animal[] animals = W5L4S08.deserializeAnimalArray(data);
        });
    }
}
