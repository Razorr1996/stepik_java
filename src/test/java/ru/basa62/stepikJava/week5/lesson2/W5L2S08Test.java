package ru.basa62.stepikJava.week5.lesson2;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.*;

class W5L2S08Test {

    @Test
    void checkSumOfStream() {
        byte[] testArray = new byte[]{0x33, 0x45, 0x01};
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(testArray)) {
            int result;
            result = assertDoesNotThrow(() -> W5L2S08.checkSumOfStream(byteArrayInputStream));
            assertEquals(71, result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}