package ru.basa62.stepikJava.week5.lesson2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class W5L2S09Test {
    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;

    @BeforeEach
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private void provideInput(byte[] bytes) {
        testIn = new ByteArrayInputStream(bytes);
        System.setIn(testIn);
    }

    @AfterEach
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    @Test
    public void testMain() {
        final byte[] testBytes = new byte[]{65, 13, 10, 10, 13};
        final byte[] expectedBytes = new byte[]{65, 10, 10, 13};
        provideInput(testBytes);

        W5L2S09.Main.main(new String[0]);

        assertArrayEquals(expectedBytes, testOut.toByteArray());
    }
}
