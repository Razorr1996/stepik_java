package ru.basa62.stepikJava.week5.lesson3;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

class W5L3S12Test {

    @Test
    void readAsString() {
        byte[] testArray = new byte[]{48, 49, 50, 51};
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(testArray)) {
            String result;
            result = assertDoesNotThrow(() -> W5L3S12.readAsString(byteArrayInputStream, StandardCharsets.US_ASCII));
            assertEquals("0123", result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
