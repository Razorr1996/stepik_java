package ru.basa62.stepikJava.week6.lesson4;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class W6L4S12Test {

    @Test
    void findMinMax() {
        Stream<Integer> stream = Arrays.stream(new Integer[]{10, 20, 1, 5, 8, 94, 1, -52, 0});
        Comparator<Integer> comparator = Integer::compare;
        final int[] biConsumerAcceptCount = {0};
        BiConsumer<Integer, Integer> biConsumer = (min, max) -> {
            assertEquals(new Integer(-52), min);
            assertEquals(new Integer(94), max);
            biConsumerAcceptCount[0] += 1;
        };

        W6L4S12.findMinMax(stream, comparator, biConsumer);

        assertEquals(1, biConsumerAcceptCount[0], "biConsumer didn't accept");
    }
}