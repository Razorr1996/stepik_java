package ru.basa62.stepikJava.week6.lesson4.W6L4S13;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class W6L4S13Test {
    @Test
    void testTopCountLorem() {
        ByteArrayInputStream inputStream = new ByteArrayInputStream("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sodales consectetur purus at faucibus. Donec mi quam, tempor vel ipsum non, faucibus suscipit massa. Morbi lacinia velit blandit tincidunt efficitur. Vestibulum eget metus imperdiet sapien laoreet faucibus. Nunc eget vehicula mauris, ac auctor lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel odio nec mi tempor dignissim.".getBytes());
        List<String> actual = Main.topCount(inputStream);
        List<String> expected = Arrays.asList(
                "consectetur",
                "faucibus",
                "ipsum",
                "lorem",
                "adipiscing",
                "amet",
                "dolor",
                "eget",
                "elit",
                "mi"
        );
        assertEquals(expected, actual);
    }

    @Test
    void testTopCountMama() {
        ByteArrayInputStream inputStream = new ByteArrayInputStream("Мама мыла-мыла-мыла раму!".getBytes());
        List<String> actual = Main.topCount(inputStream);
        List<String> expected = Arrays.asList(
                "мыла",
                "мама",
                "раму"
        );
        assertEquals(expected, actual);
    }
}
