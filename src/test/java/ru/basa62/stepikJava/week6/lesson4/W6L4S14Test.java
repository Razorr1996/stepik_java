package ru.basa62.stepikJava.week6.lesson4;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static ru.basa62.stepikJava.week6.lesson4.W6L4S14.*;

@SuppressWarnings({"SimplifyStreamApiCallChains", "ArraysAsListWithZeroOrOneArgument"})
class W6L4S14Test {
    // Random variables
    String randomFrom = "Artem"; // Некоторая случайная строка. Можете выбрать ее самостоятельно.
    String randomTo = "Mari";  // Некоторая случайная строка. Можете выбрать ее самостоятельно.
    int randomSalary = 100;  // Некоторое случайное целое положительное число. Можете выбрать его самостоятельно.

    @Test
    void testMailMessage() {
        // Создание списка из трех почтовых сообщений.
        MailMessage firstMessage = new MailMessage(
                "Robert Howard",
                "H.P. Lovecraft",
                "This \"The Shadow over Innsmouth\" story is real masterpiece, Howard!"
        );

        assertEquals("Robert Howard", firstMessage.getFrom(), "Wrong firstMessage from address");
        assertEquals("H.P. Lovecraft", firstMessage.getTo(), "Wrong firstMessage to address");
        assertTrue(firstMessage.getContent().endsWith("Howard!"), "Wrong firstMessage content ending");

        MailMessage secondMessage = new MailMessage(
                "Jonathan Nolan",
                "Christopher Nolan",
                "Брат, почему все так хвалят только тебя, когда практически все сценарии написал я. Так не честно!"
        );

        MailMessage thirdMessage = new MailMessage(
                "Stephen Hawking",
                "Christopher Nolan",
                "Я так и не понял Интерстеллар."
        );

        List<MailMessage> messages = Arrays.asList(
                firstMessage, secondMessage, thirdMessage
        );

        // Создание почтового сервиса.
        MailService<String> mailService = new MailService<>();

        // Обработка списка писем почтовым сервисом
        messages.stream().forEachOrdered(mailService);

        // Получение и проверка словаря "почтового ящика",
        //   где по получателю можно получить список сообщений, которые были ему отправлены
        Map<String, List<String>> mailBox = mailService.getMailBox();


        assertEquals(
                Arrays.asList("This \"The Shadow over Innsmouth\" story is real masterpiece, Howard!"),
                mailBox.get("H.P. Lovecraft"),
                "wrong mailService mailbox content (1)"
        );

        assertEquals(
                Arrays.asList(
                        "Брат, почему все так хвалят только тебя, когда практически все сценарии написал я. Так не честно!",
                        "Я так и не понял Интерстеллар."
                ),
                mailBox.get("Christopher Nolan"),
                "wrong mailService mailbox content (2)"
        );
        assertEquals(Collections.<String>emptyList(), mailBox.get(randomTo), "wrong mailService mailbox content (3)");
    }

    @Test
    void testSalary() {
        // Создание списка из трех зарплат.
        Salary salary1 = new Salary("Facebook", "Mark Zuckerberg", 1);
        Salary salary2 = new Salary("FC Barcelona", "Lionel Messi", Integer.MAX_VALUE);
        Salary salary3 = new Salary(randomFrom, randomTo, randomSalary);

        // Создание почтового сервиса, обрабатывающего зарплаты.
        MailService<Integer> salaryService = new MailService<>();

        // Обработка списка зарплат почтовым сервисом
        Arrays.asList(salary1, salary2, salary3).forEach(salaryService);

        // Получение и проверка словаря "почтового ящика",
        //   где по получателю можно получить список зарплат, которые были ему отправлены.
        Map<String, List<Integer>> salaries = salaryService.getMailBox();
        assertEquals(Arrays.asList(1), salaries.get(salary1.getTo()), "wrong salaries mailbox content (1)");
        assertEquals(Arrays.asList(Integer.MAX_VALUE), salaries.get(salary2.getTo()), "wrong salaries mailbox content (2)");
        assertEquals(Arrays.asList(randomSalary), salaries.get(randomTo), "wrong salaries mailbox content (3)");
    }
}
