package ru.basa62.stepikJava.week6.lesson4;

import org.junit.jupiter.api.Test;

import java.util.OptionalInt;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class W6L4S11Test {

    @Test
    void pseudoRandomStreamDigits() {
        int testSeed = 134;
        int expected = 795;
        IntStream intStream = W6L4S11.pseudoRandomStream(testSeed);
        OptionalInt result = intStream
                .skip(1)
                .limit(1)
                .findFirst();
        assertTrue(result.isPresent());

        int actual = result.getAsInt();
        assertEquals(expected, actual);
    }

    @Test
    void pseudoRandomStream13() {
        int[] expected = new int[]{13, 16, 25, 62, 384, 745, 502, 200, 0, 0};
        IntStream intStream = W6L4S11.pseudoRandomStream(13);
        int[] actual = intStream
                .limit(10)
                .toArray();
        assertArrayEquals(expected, actual);
    }
}
