package ru.basa62.stepikJava.week6.lesson2.W6L2S15;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

class W6L2S15Test {

    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;

    @BeforeEach
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private void provideInput(String s) {
        testIn = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8));
        System.setIn(testIn);
    }

    @AfterEach
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    @Test
    void testMain() {
        final String testIn = "1 2 3 4 5 6 7";
        final String expectedOut = "6 4 2";

        provideInput(testIn);

        Main.main(new String[]{});

        String actualOut = testOut.toString().trim();

        assertEquals(expectedOut, actualOut);
    }
}