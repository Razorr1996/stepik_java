package ru.basa62.stepikJava.week6.lesson2;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class W6L2S14Test {

    @Test
    void symmetricDifference() {
        Set<Integer> num1 = new HashSet<>();
        num1.add(3);
        num1.add(7);
        num1.add(9);

        HashSet<Integer> num2 = new HashSet<>();
        num2.add(5);
        num2.add(7);
        num2.add(12);

        HashSet<Integer> expectedResult = new HashSet<>();
        expectedResult.add(3);
        expectedResult.add(9);
        expectedResult.add(5);
        expectedResult.add(12);

        Set<Integer> actualResult = W6L2S14.symmetricDifference(num1, num2);
        assertEquals(expectedResult, actualResult);
    }
}