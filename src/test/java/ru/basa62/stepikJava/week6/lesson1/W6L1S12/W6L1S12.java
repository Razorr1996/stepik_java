package ru.basa62.stepikJava.week6.lesson1.W6L1S12;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W6L1S12Test {
    @Test
    void testPair() {
        Pair<Integer, String> pair = Pair.of(1, "hello");

        Integer i = pair.getFirst(); // 1
        assertEquals(1, i);

        String s = pair.getSecond(); // "hello"
        assertEquals("hello", s);

        Pair<Integer, String> pair2 = Pair.of(1, "hello");

        assertEquals(pair, pair2);

        assertEquals(pair.hashCode(), pair2.hashCode());
    }

    @Test
    void testPairNull() {
        assertDoesNotThrow(() -> {
            Pair<?, ?> p = Pair.of(null, null);
            assertNull(p.getFirst());
            assertNull(p.getSecond());
        });
    }
}