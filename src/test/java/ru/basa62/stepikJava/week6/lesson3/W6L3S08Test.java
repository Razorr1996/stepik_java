package ru.basa62.stepikJava.week6.lesson3;

import org.junit.jupiter.api.Test;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

class W6L3S08Test {

    @Test
    void ternaryOperator() {
        Predicate<Object> condition = Objects::isNull;
        Function<Object, Integer> ifTrue = obj -> 0;
        Function<CharSequence, Integer> ifFalse = CharSequence::length;
        Function<String, Integer> safeStringLength = W6L3S08.ternaryOperator(condition, ifTrue, ifFalse);
        assertEquals(0, safeStringLength.apply(null));
        assertEquals(0, safeStringLength.apply(""));
        assertEquals(5, safeStringLength.apply("Artem"));
    }
}