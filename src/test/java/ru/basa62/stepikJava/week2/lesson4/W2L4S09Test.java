package ru.basa62.stepikJava.week2.lesson4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W2L4S09Test {

    @Test
    void mergeArrays() {
        assertArrayEquals(
                new int[]{0, 1, 2, 2, 3},
                W2L4S09.mergeArrays(
                        new int[]{0, 2, 2},
                        new int[]{1, 3}
                )
        );
        assertArrayEquals(
                new int[]{},
                W2L4S09.mergeArrays(
                        new int[]{},
                        new int[]{}
                )
        );
        assertArrayEquals(
                new int[]{1},
                W2L4S09.mergeArrays(
                        new int[]{1},
                        new int[]{}
                )
        );
        assertArrayEquals(
                new int[]{1},
                W2L4S09.mergeArrays(
                        new int[]{},
                        new int[]{1}
                )
        );
    }
}