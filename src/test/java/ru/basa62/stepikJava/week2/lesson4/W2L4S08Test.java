package ru.basa62.stepikJava.week2.lesson4;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class W2L4S08Test {

    @Test
    void factorial() {
        assertEquals(BigInteger.valueOf(1), W2L4S08.factorial(1));
        assertEquals(BigInteger.valueOf(6), W2L4S08.factorial(3));
    }
}
