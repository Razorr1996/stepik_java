package ru.basa62.stepikJava.week2.lesson1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W2L1S12Test {

    @Test
    void doubleExpression() {
        assertTrue(W2L1S12.doubleExpression(0.1, 0.2, 0.3));
        assertTrue(W2L1S12.doubleExpression(0.1, 0.1, 0.2));
        assertTrue(W2L1S12.doubleExpression(0.0, 0.0, 0.0));
    }
}