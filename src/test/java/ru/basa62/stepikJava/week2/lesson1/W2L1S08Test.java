package ru.basa62.stepikJava.week2.lesson1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class W2L1S08Test {

    @Test
    void leapYearCount() {
        assertEquals(0, W2L1S08.leapYearCount(1));
        assertEquals(1, W2L1S08.leapYearCount(4));
        assertEquals(24, W2L1S08.leapYearCount(100));
    }
}
