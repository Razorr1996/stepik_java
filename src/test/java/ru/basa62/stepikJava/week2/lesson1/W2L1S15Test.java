package ru.basa62.stepikJava.week2.lesson1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W2L1S15Test {

    @Test
    void flipBit() {
        assertEquals(1, W2L1S15.flipBit(0, 1));
        assertEquals(2, W2L1S15.flipBit(0, 2));
        assertEquals(4, W2L1S15.flipBit(0, 3));
        assertEquals(8, W2L1S15.flipBit(0, 4));

        assertEquals(Integer.MIN_VALUE, W2L1S15.flipBit(0, 32));
    }
}