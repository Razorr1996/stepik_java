package ru.basa62.stepikJava.week2.lesson2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W2L2S03Test {

    @Test
    void charExpression() {
        assertEquals('|', W2L2S03.charExpression(32));
        assertEquals('y', W2L2S03.charExpression(29));
    }
}
