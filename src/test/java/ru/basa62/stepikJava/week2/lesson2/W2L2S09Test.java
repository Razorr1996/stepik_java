package ru.basa62.stepikJava.week2.lesson2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W2L2S09Test {

    @Test
    void isPowerOfTwo() {
        assertFalse(W2L2S09.isPowerOfTwo(0));
        assertTrue(W2L2S09.isPowerOfTwo(1));
        assertTrue(W2L2S09.isPowerOfTwo(-2));
    }
}