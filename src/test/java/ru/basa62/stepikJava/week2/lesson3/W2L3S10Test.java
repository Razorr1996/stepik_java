package ru.basa62.stepikJava.week2.lesson3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class W2L3S10Test {

    @Test
    void isPalindrome() {
        assertTrue(W2L3S10.isPalindrome("Madam, I'm Adam!"));
    }
}