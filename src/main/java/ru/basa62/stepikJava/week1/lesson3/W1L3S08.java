package ru.basa62.stepikJava.week1.lesson3;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class W1L3S08 {
    public static void main(String[] args) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] digest = md.digest("abracadabra".getBytes(StandardCharsets.UTF_8));
        for (byte b : digest) {
            System.out.printf("%02x", b);
        }
        System.out.println();
    }
}
