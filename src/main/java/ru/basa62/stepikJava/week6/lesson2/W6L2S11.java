package ru.basa62.stepikJava.week6.lesson2;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("ALL")
public class W6L2S11 {
    public static void main(String[] args) {
        Collection<?> collection = new ArrayList<>();
        Object object = 0;

        collection.toArray();

        collection.remove(object);

//        collection.addAll(Arrays.asList(object));

//        collection.add(object);

        collection.iterator();

        collection.contains(object);

        collection.clear();

        collection.size();

    }
}
