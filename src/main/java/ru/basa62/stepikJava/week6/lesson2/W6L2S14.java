package ru.basa62.stepikJava.week6.lesson2;

import java.util.HashSet;
import java.util.Set;

public class W6L2S14 {
    public static <T> Set<T> symmetricDifference(Set<? extends T> set1, Set<? extends T> set2) {
        if (set1 == null && set2 == null) {
            return new HashSet<>();
        } else if (set1 == null || set2 == null) {
            if (set1 == null)
                return new HashSet<>(set2);
            else
                return new HashSet<>(set1);
        }

        Set<T> result = new HashSet<>();
        for (T element : set1) {
            if (!set2.contains(element))
                result.add(element);
        }
        for (T element : set2) {
            if (!set1.contains(element))
                result.add(element);
        }

        return result;
    }
}
