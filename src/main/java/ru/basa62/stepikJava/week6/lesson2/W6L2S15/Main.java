package ru.basa62.stepikJava.week6.lesson2.W6L2S15;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int pos = 0;

        List<Integer> list = new ArrayList<>();

        while (scanner.hasNext()) {
            int t = scanner.nextInt();
            if (pos % 2 != 0)
                list.add(t);
            pos++;
        }
        Collections.reverse(list);

        StringBuilder stringBuilder = new StringBuilder();
        for (Integer e : list) {
            stringBuilder.append(e).append(" ");
        }

        System.out.println(stringBuilder.toString().trim());
    }
}
