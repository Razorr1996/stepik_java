package ru.basa62.stepikJava.week6.lesson3;

import java.util.function.Function;
import java.util.function.Predicate;

public class W6L3S08 {
    public static <T, U> Function<T, U> ternaryOperator(
            Predicate<? super T> condition,
            Function<? super T, ? extends U> ifTrue,
            Function<? super T, ? extends U> ifFalse) {

        return (x) -> condition.test(x) ? ifTrue.apply(x) : ifFalse.apply(x);
    }
}
