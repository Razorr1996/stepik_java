package ru.basa62.stepikJava.week6.lesson4;

import java.util.stream.IntStream;

public class W6L4S11 {
    public static IntStream pseudoRandomStream(int seed) {
        return IntStream.iterate(seed, x -> (x * x) / 10 % 1000);
    }
}
