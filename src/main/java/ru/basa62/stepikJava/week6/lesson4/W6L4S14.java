package ru.basa62.stepikJava.week6.lesson4;

import java.util.*;
import java.util.function.*;

public class W6L4S14 {
    //region Task
    public static abstract class Sendable<T> {
        private final String from;
        private final String to;
        private final T content;

        public Sendable(String from, String to, T content) {
            this.from = from;
            this.to = to;
            this.content = content;
        }

        public String getFrom() {
            return from;
        }

        public String getTo() {
            return to;
        }

        public T getContent() {
            return content;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Sendable)) return false;
            Sendable<?> sendable = (Sendable<?>) o;
            return Objects.equals(getFrom(), sendable.getFrom()) && Objects.equals(getTo(), sendable.getTo()) && Objects.equals(getContent(), sendable.getContent());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getFrom(), getTo(), getContent());
        }
    }

    public static class MailMessage extends Sendable<String> {
        public MailMessage(String from, String to, String content) {
            super(from, to, content);
        }
    }

    public static class Salary extends Sendable<Integer> {
        public Salary(String from, String to, Integer content) {
            super(from, to, content);
        }
    }

    public static class MailService<T> implements Consumer<Sendable<T>> {
        private final Map<String, List<T>> result = new HashMap<String, List<T>>() {
            @Override
            public List<T> get(Object key) {
                return super.getOrDefault(key, new ArrayList<>());
            }
        };

        @Override
        public void accept(Sendable<T> tSendable) {
            List<T> list = result.get(tSendable.getTo());
            list.add(tSendable.getContent());
            result.put(tSendable.getTo(), list);
        }

        public Map<String, List<T>> getMailBox() {
            return result;
        }
    }
    //endregion
}
