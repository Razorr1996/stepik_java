package ru.basa62.stepikJava.week6.lesson4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class W6L4S12 {
    public static <T> void findMinMax(
            Stream<? extends T> stream,
            Comparator<? super T> order,
            BiConsumer<? super T, ? super T> minMaxConsumer) {

        @SuppressWarnings({"unchecked"})
        T[] t = (T[]) stream.toArray();

        T minValue = Arrays.stream(t).min(order).orElse(null);
        T maxValue = Arrays.stream(t).max(order).orElse(null);

        minMaxConsumer.accept(minValue, maxValue);
    }
}
