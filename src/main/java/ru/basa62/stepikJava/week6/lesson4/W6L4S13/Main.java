package ru.basa62.stepikJava.week6.lesson4.W6L4S13;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Main {
    public static List<String> topCount(InputStream inputStream, int n) {
        Iterable<String> it = () -> new Scanner(inputStream, StandardCharsets.UTF_8.name());

        Pattern pattern = Pattern.compile("[a-zа-яё0-9]+");

        Comparator<Map.Entry<String, Long>> entryComparator = Collections.reverseOrder(Map.Entry.comparingByValue());

        return StreamSupport.stream(it.spliterator(), false)
                .map(String::toLowerCase)
                .flatMap(s -> {
                    Matcher matcher = pattern.matcher(s);

                    List<String> allMatches = new ArrayList<>();

                    while (matcher.find()) {
                        allMatches.add(matcher.group());
                    }

                    return allMatches.stream();
                })
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Comparator.<Map.Entry<String, Long>>comparingLong(Map.Entry::getValue).reversed().thenComparing(Map.Entry::getKey))
                .limit(n)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static List<String> topCount(InputStream inputStream) {
        return topCount(inputStream, 10);
    }

    public static void main(String[] args) {
        topCount(System.in).forEach(System.out::println);
    }
}
