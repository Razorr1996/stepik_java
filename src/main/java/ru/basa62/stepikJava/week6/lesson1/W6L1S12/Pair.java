package ru.basa62.stepikJava.week6.lesson1.W6L1S12;

import java.util.Objects;

class Pair<T1, T2> {
    private final T1 first;
    private final T2 second;

    private static final Pair<?, ?> EMPTY = new Pair<>();

    private Pair() {
        this.first = null;
        this.second = null;
    }

    public static <T1, T2> Pair<T1, T2> empty() {
        @SuppressWarnings("unchecked")
        Pair<T1, T2> p = (Pair<T1, T2>) EMPTY;
        return p;
    }

    private Pair(T1 first, T2 second) {
        this.first = first;
        this.second = second;
    }

    public static <T1, T2> Pair<T1, T2> of(T1 first, T2 second) {
        return new Pair<>(first, second);
    }

    public T1 getFirst() {
        return first;
    }

    public T2 getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(first, pair.first) && Objects.equals(second, pair.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }
}
