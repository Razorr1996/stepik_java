package ru.basa62.stepikJava.week4.lesson3;

import java.util.logging.*;

public class W4L3S08 {
    private static void configureLogging() {
        Logger loggerClassA = Logger.getLogger("org.stepic.java.logging.ClassA");
        loggerClassA.setLevel(Level.ALL);

        Logger loggerClassB = Logger.getLogger("org.stepic.java.logging.ClassB");
        loggerClassB.setLevel(Level.WARNING);

        Formatter xmlFormatter = new XMLFormatter();
        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.ALL);
        consoleHandler.setFormatter(xmlFormatter);
        Logger loggerJava = Logger.getLogger("org.stepic.java");
        loggerJava.setUseParentHandlers(false);
        loggerJava.addHandler(consoleHandler);
    }
}
