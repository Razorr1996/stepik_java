package ru.basa62.stepikJava.week4.lesson1;

public class W4L1S10 {
    public static String getCallerClassAndMethodName() {
        StackTraceElement[] elements = new Throwable().getStackTrace();
        if (elements.length > 2) {
            StackTraceElement element = elements[2];
            return element.getClassName() + "#" + element.getMethodName();
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(getCallerClassAndMethodName());
        m1();
    }

    static void m1() {
        System.out.println(getCallerClassAndMethodName());
        m2();
    }

    static void m2() {
        System.out.println(getCallerClassAndMethodName());
    }
}
