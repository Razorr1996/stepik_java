package ru.basa62.stepikJava.week4.lesson1;

public class W4L1S09 {
    public static double sqrt(double x) {
        if (x < 0) {
            throw new IllegalArgumentException("Expected non-negative number, got " + x);
        }
        return Math.sqrt(x);
    }
}
