package ru.basa62.stepikJava.week4.lesson1;

public class W4L1S08 {
    @SuppressWarnings({"WrapperTypeMayBePrimitive", "UnnecessaryLocalVariable", "RedundantCast", "ConstantConditions", "unused"})
    public static void main(String[] args) {
        Integer x = 1;
        Object o = (Object) x;
        String s = (String) o;
    }
}
