package ru.basa62.stepikJava.week4.lesson2.W4L2S07;

public interface RobotConnectionManager {
    RobotConnection getConnection();
}
