package ru.basa62.stepikJava.week4.lesson2.W4L2S07;

public interface RobotConnection extends AutoCloseable {
    void moveRobotTo(int x, int y);

    @Override
    void close();
}
