package ru.basa62.stepikJava.week4.lesson2.W4L2S07;

public class W4L2S07 {
    public static void moveRobot(RobotConnectionManager robotConnectionManager, int toX, int toY) {
        final int maxRetryCount = 3;

        int retryCount = 0;
        boolean success = false;

        while (retryCount < maxRetryCount && !success) {
            try (RobotConnection robotConnection = robotConnectionManager.getConnection()) {
                robotConnection.moveRobotTo(toX, toY);
                success = true;
            } catch (RobotConnectionException e) {
                retryCount += 1;
            }
        }
        if (!success) {
            throw new RobotConnectionException("");
        }
    }
}
