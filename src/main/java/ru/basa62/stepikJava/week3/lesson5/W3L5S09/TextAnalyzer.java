package ru.basa62.stepikJava.week3.lesson5.W3L5S09;

interface TextAnalyzer {
    Label processText(String text);
}
