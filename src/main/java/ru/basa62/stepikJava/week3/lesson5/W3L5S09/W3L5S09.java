package ru.basa62.stepikJava.week3.lesson5.W3L5S09;

public class W3L5S09 {
    public Label checkLabels(TextAnalyzer[] analyzers, String text) {
        for (TextAnalyzer analyzer : analyzers) {
            Label currentProcess = analyzer.processText(text);
            if (currentProcess != Label.OK) {
                return currentProcess;
            }
        }
        return Label.OK;
    }
}
