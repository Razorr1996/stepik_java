package ru.basa62.stepikJava.week3.lesson5;

import java.util.function.DoubleUnaryOperator;

public class W3L5S07 {
    public static double integrate(DoubleUnaryOperator f, double a, double b) {
        double delta = b - a;
        int stepCount = 1;
        double stepSize = delta;

        while (Math.abs(stepSize) >= 1e-6) {
            stepCount *= 2;
            stepSize = delta / stepCount;
        }

        double result = 0;

        for (int i = 0; i < stepCount; i++) {
            double currentX = a + (i + 0.5) * stepSize;
            double currentY = f.applyAsDouble(currentX);

            result += currentY * stepSize;
        }

        return result;
    }
}
