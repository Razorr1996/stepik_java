package ru.basa62.stepikJava.week3.lesson5.W3L5S09;

public class TooLongTextAnalyzer implements TextAnalyzer {
    private final int maxLength;

    public TooLongTextAnalyzer(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public Label processText(String text) {
        return (text.length() <= maxLength) ? Label.OK : Label.TOO_LONG;
    }
}
