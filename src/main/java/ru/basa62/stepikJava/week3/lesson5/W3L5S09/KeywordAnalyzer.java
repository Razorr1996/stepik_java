package ru.basa62.stepikJava.week3.lesson5.W3L5S09;

abstract class KeywordAnalyzer implements TextAnalyzer {
    protected abstract Label getLabel();

    protected abstract String[] getKeywords();

    @Override
    public Label processText(String text) {
        for (String keyword : getKeywords()) {
            if (text.contains(keyword)) {
                return getLabel();
            }
        }
        return Label.OK;
    }
}
