package ru.basa62.stepikJava.week3.lesson5;

import java.util.Arrays;

public class W3L5S08 {
    public static class AsciiCharSequence implements CharSequence {
        private final byte[] bytes;

        public AsciiCharSequence(byte[] input) {
            this.bytes = input.clone();
        }

        @Override
        public int length() {
            return bytes.length;
        }

        @Override
        public char charAt(int index) {
            return (char) bytes[index];
        }

        @Override
        public AsciiCharSequence subSequence(int start, int end) {
            return new AsciiCharSequence(Arrays.copyOfRange(bytes, start, end));
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            for (byte b : bytes) {
                stringBuilder.append((char) b);
            }
            return stringBuilder.toString();
        }
    }
}
