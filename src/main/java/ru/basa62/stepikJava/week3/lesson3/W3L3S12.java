package ru.basa62.stepikJava.week3.lesson3;

public class W3L3S12 {
    public static void moveRobot(Robot robot, int toX, int toY) {
        int deltaX = toX - robot.getX();
        if (0 != deltaX) {
            if (deltaX > 0) {
                turnRobot(robot, Direction.RIGHT);
                for (int i = 0; i < deltaX; i++) {
                    robot.stepForward();
                }
            } else {
                turnRobot(robot, Direction.LEFT);
                for (int i = 0; i < -deltaX; i++) {
                    robot.stepForward();
                }
            }
        }

        int deltaY = toY - robot.getY();
        if (0 != deltaY) {
            if (deltaY > 0) {
                turnRobot(robot, Direction.UP);
                for (int i = 0; i < deltaY; i++) {
                    robot.stepForward();
                }
            } else {
                turnRobot(robot, Direction.DOWN);
                for (int i = 0; i < -deltaY; i++) {
                    robot.stepForward();
                }
            }
        }
    }

    public static void turnRobot(Robot robot, Direction to) {
        final Direction from = robot.getDirection();

        if (from != to) {
            if ((Direction.UP == from && Direction.DOWN == to) ||
                    (Direction.DOWN == from && Direction.UP == to) ||
                    (Direction.LEFT == from && Direction.RIGHT == to) ||
                    (Direction.RIGHT == from && Direction.LEFT == to)) {
                robot.turnRight();
                robot.turnRight();
            } else if ((Direction.UP == from && Direction.RIGHT == to) ||
                    (Direction.DOWN == from && Direction.LEFT == to) ||
                    (Direction.LEFT == from && Direction.UP == to) ||
                    (Direction.RIGHT == from && Direction.DOWN == to)) {
                robot.turnRight();
            } else if ((Direction.UP == from && Direction.LEFT == to) ||
                    (Direction.DOWN == from && Direction.RIGHT == to) ||
                    (Direction.LEFT == from && Direction.DOWN == to) ||
                    (Direction.RIGHT == from && Direction.UP == to)) {
                robot.turnLeft();
            }
        }
    }
}
