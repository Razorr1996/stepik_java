package ru.basa62.stepikJava.week3.lesson3;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
