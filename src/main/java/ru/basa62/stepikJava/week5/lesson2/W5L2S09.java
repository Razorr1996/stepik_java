package ru.basa62.stepikJava.week5.lesson2;

public class W5L2S09 {
    public static class Main {
        public static void main(String[] args) {
            try {
                int b = System.in.read();
                if (b != -1) {
                    byte prev = (byte) b;
                    byte cur;
                    b = System.in.read();
                    while (b != -1) {
                        cur = (byte) b;
                        if (!(prev == 13 && cur == 10)) {
                            System.out.write(prev);
                        }
                        prev = cur;
                        b = System.in.read();
                    }
                    System.out.write(prev);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.flush();
        }
    }
}
