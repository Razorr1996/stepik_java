package ru.basa62.stepikJava.week5.lesson2;

import java.io.IOException;
import java.io.InputStream;

public class W5L2S08 {
    public static int checkSumOfStream(InputStream inputStream) throws IOException {
        int result = 0;
        int b = inputStream.read();
        while (b != -1) {
            result = Integer.rotateLeft(result, 1) ^ b;
            b = inputStream.read();
        }
        return result;
    }
}
