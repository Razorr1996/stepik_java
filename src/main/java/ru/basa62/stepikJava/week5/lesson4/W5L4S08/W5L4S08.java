package ru.basa62.stepikJava.week5.lesson4.W5L4S08;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class W5L4S08 {
    public static Animal[] deserializeAnimalArray(byte[] data) {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(data))) {
            final int size = objectInputStream.readInt();
            if (size < 0) {
                throw new IllegalArgumentException("Size less 0");
            }
            if (size == 0) {
                return new Animal[]{};
            }
            Animal[] result = new Animal[size];
            for (int i = 0; i < size; i++) {
                result[i] = (Animal) objectInputStream.readObject();
            }
            return result;
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
