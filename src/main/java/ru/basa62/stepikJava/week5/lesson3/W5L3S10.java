package ru.basa62.stepikJava.week5.lesson3;

import java.nio.charset.StandardCharsets;

public class W5L3S10 {
    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b :
                "Ы".getBytes(StandardCharsets.UTF_8)) {
            stringBuilder
                    .append(Byte.toUnsignedInt(b))
                    .append(" ");
        }
        System.out.println(stringBuilder.toString().trim());
    }
}
