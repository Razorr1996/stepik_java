package ru.basa62.stepikJava.week5.lesson3;

import java.util.InputMismatchException;
import java.util.Scanner;


//public class Main {
public class W5L3S13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double result = 0.0;

        while (scanner.hasNext()) {
            try {
                if (scanner.hasNextDouble()) {
                    result += scanner.nextDouble();
                } else {
                    scanner.next();
                }
            } catch (InputMismatchException e) {
                scanner.next();
            }
        }

        System.out.printf("%.6f", result);
    }
}
