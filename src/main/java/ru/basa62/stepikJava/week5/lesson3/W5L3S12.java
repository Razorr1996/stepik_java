package ru.basa62.stepikJava.week5.lesson3;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class W5L3S12 {
    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        StringBuilder builder = new StringBuilder();

        try (InputStreamReader reader = new InputStreamReader(inputStream, charset)) {
            int i;
            while (-1 != (i = reader.read())) {
                builder.append((char) i);
            }
        }

        return builder.toString();
    }
}
