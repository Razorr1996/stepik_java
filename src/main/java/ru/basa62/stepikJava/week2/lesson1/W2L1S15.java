package ru.basa62.stepikJava.week2.lesson1;

public class W2L1S15 {
    /**
     * Flips one bit of the given <code>value</code>.
     *
     * @param value    any number
     * @param bitIndex index of the bit to flip, 1 <= bitIndex <= 32
     * @return new value with one bit flipped
     */
    public static int flipBit(int value, int bitIndex) {
        int changer = 1 << (bitIndex - 1);
        return value ^ changer;
    }
}
