package ru.basa62.stepikJava.week2.lesson1;

public class W2L1S12 {
    public static boolean doubleExpression(double a, double b, double c) {
        return Math.abs(c - a - b) < 1e-4;
    }
}
