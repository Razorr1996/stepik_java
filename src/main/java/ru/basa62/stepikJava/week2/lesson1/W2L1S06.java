package ru.basa62.stepikJava.week2.lesson1;

public class W2L1S06 {
    public static boolean booleanExpression(boolean a, boolean b, boolean c, boolean d) {
        int countTrue = 0;
        countTrue += a ? 1 : 0;
        countTrue += b ? 1 : 0;
        countTrue += c ? 1 : 0;
        countTrue += d ? 1 : 0;
        return countTrue == 2;
    }
}
