package ru.basa62.stepikJava.week2.lesson1;

public class W2L1S08 {
    public static int leapYearCount(int year) {
        int years_4 = year / 4;
        int years_100 = year / 100;
        int years_400 = year / 400;
        return years_4 - years_100 + years_400;
    }
}
