package ru.basa62.stepikJava.week2.lesson3;

public class W2L3S10 {
    /**
     * Checks if given <code>text</code> is a palindrome.
     *
     * @param text any string
     * @return <code>true</code> when <code>text</code> is a palindrome, <code>false</code> otherwise
     */
    public static boolean isPalindrome(String text) {
        String clearLetters = text.replaceAll("[^a-zA-Z0-9]", "");
        String clearLettersReversed = new StringBuilder(clearLetters).reverse().toString();
        return clearLetters.equalsIgnoreCase(clearLettersReversed);
    }
}
