package ru.basa62.stepikJava.week2.lesson3;

public class W2L3S08 {
    public static void main(String[] args) {
        System.out.println("A" + 12);
        System.out.println('A' + '1' + "2");
        System.out.println("A" + ('\t' + '\u0003'));
        System.out.println('A' + "12");
    }
}
