package ru.basa62.stepikJava.week2.lesson2;

public class W2L2S09 {
    /**
     * Checks if given <code>value</code> is a power of two.
     *
     * @param value any number
     * @return <code>true</code> when <code>value</code> is power of two, <code>false</code> otherwise
     */
    public static boolean isPowerOfTwo(int value) {
        return 1 == Integer.bitCount(Math.abs(value));
    }
}
