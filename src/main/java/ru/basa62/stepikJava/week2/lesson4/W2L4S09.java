package ru.basa62.stepikJava.week2.lesson4;

public class W2L4S09 {
    /**
     * Merges two given sorted arrays into one
     *
     * @param a1 first sorted array
     * @param a2 second sorted array
     * @return new array containing all elements from a1 and a2, sorted
     */
    public static int[] mergeArrays(int[] a1, int[] a2) {
        int[] result = new int[a1.length + a2.length];
        for (int i1 = 0, i2 = 0; i1 + i2 < result.length; ) {
            if (i1 < a1.length && i2 < a2.length) {
                if (a1[i1] <= a2[i2]) {
                    result[i1 + i2] = a1[i1];
                    i1++;
                } else {
                    result[i1 + i2] = a2[i2];
                    i2++;
                }
            } else if (i1 < a1.length) {
                result[i1 + i2] = a1[i1];
                i1++;
            } else {
                result[i1 + i2] = a2[i2];
                i2++;
            }
        }
        return result;
    }
}
