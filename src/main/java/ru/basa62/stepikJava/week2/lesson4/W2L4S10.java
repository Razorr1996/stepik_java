package ru.basa62.stepikJava.week2.lesson4;

import java.util.HashMap;
import java.util.Map;

public class W2L4S10 {
    private String printTextPerRole(String[] roles, String[] textLines) {
        Map<String, StringBuilder> roleBuilders = new HashMap<>();
        for (String role : roles) {
            roleBuilders.put(role, new StringBuilder());
        }
        for (int i = 0; i < textLines.length; i++) {
            String text = textLines[i];
            int num = i + 1;
            String currentRole = null;
            String currentText = null;
            for (String role :
                    roles) {
                if (text.startsWith(role + ":")) {
                    currentRole = role;
                    currentText = text.replaceFirst(role + ": ", "");
                    break;
                }
            }
            if (currentRole != null) {
                roleBuilders.get(currentRole)
                        .append(i + 1)
                        .append(") ")
                        .append(currentText)
                        .append("\n");
            }
        }
        StringBuilder result = new StringBuilder();
        for (String role : roles) {
            result
                    .append(role)
                    .append(":\n")
                    .append(roleBuilders.get(role)).append("\n");
        }
        return result.toString().trim();
    }

    public static String printTextPerRolePublic(String[] roles, String[] textLines) {
        return new W2L4S10().printTextPerRole(roles, textLines);
    }
}
