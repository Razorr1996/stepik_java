package ru.basa62.stepikJava.week2.lesson4;

import java.math.BigInteger;

public class W2L4S08 {
    /**
     * Calculates factorial of given <code>value</code>.
     *
     * @param value positive number
     * @return factorial of <code>value</code>
     */
    public static BigInteger factorial(int value) {
        BigInteger result = new BigInteger("1");
        for (int i = 1; i < value + 1; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }
}
